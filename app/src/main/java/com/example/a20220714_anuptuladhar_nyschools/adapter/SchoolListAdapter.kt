package com.example.a20220714_anuptuladhar_nyschools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220714_anuptuladhar_nyschools.databinding.RowSchoolBinding
import com.example.a20220714_anuptuladhar_nyschools.models.SchoolResponseItem

class SchoolListAdapter(private val onItemSelection: (SchoolResponseItem) -> Unit) :
    ListAdapter<SchoolResponseItem, RecyclerView.ViewHolder>(SchoolDiffUtilCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: RowSchoolBinding =
            RowSchoolBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SchoolViewHolder).bind(getItem(position), onItemSelection)
    }

    class SchoolViewHolder(private val binding: RowSchoolBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: SchoolResponseItem, onItemSelection: (SchoolResponseItem) -> Unit) {
            binding.apply {
                titleTextView.text = data.school_name
                bodyTextView.text = data.school_email

                detailButton.setOnClickListener {
                    onItemSelection(data)
                }
            }
        }
    }
}

/**
 * Diff util to check whether the items are same or not.
 * if different new view needs to be rendered
 */
class SchoolDiffUtilCallBack : DiffUtil.ItemCallback<SchoolResponseItem>() {
    override fun areItemsTheSame(
        oldItem: SchoolResponseItem,
        newItem: SchoolResponseItem
    ): Boolean {
        return oldItem.dbn == newItem.dbn
    }

    override fun areContentsTheSame(
        oldItem: SchoolResponseItem,
        newItem: SchoolResponseItem
    ): Boolean {
        return oldItem == newItem
    }
}

