package com.example.a20220714_anuptuladhar_nyschools.api

import com.example.a20220714_anuptuladhar_nyschools.models.SchoolResponse
import com.example.a20220714_anuptuladhar_nyschools.models.details.SchoolDetailsResponse
import retrofit2.Response

interface ApiHelper {

    suspend fun fetchSchoolList(): Response<SchoolResponse>

    suspend fun fetchSatScore(dbn: String): Response<SchoolDetailsResponse>
}