package com.example.a20220714_anuptuladhar_nyschools.api

import com.example.a20220714_anuptuladhar_nyschools.models.SchoolResponse
import com.example.a20220714_anuptuladhar_nyschools.models.details.SchoolDetailsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("s3k6-pzi2.json")
    suspend fun fetchSchoolList(): Response<SchoolResponse>

    @GET("f9bf-2cp4.json")
    suspend fun fetchSatScore(@Query("dbn") dbn: String): Response<SchoolDetailsResponse>
}