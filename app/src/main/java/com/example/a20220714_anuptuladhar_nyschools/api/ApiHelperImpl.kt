package com.example.a20220714_anuptuladhar_nyschools.api

import com.example.a20220714_anuptuladhar_nyschools.models.SchoolResponse
import com.example.a20220714_anuptuladhar_nyschools.models.details.SchoolDetailsResponse
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun fetchSchoolList(): Response<SchoolResponse> {
        return apiService.fetchSchoolList()
    }

    override suspend fun fetchSatScore(dbn: String): Response<SchoolDetailsResponse> {
        return apiService.fetchSatScore(dbn)
    }
}