package com.example.a20220714_anuptuladhar_nyschools.repository

import com.example.a20220714_anuptuladhar_nyschools.api.ApiHelper
import javax.inject.Inject

class SchoolRepository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun fetchSchoolList() = apiHelper.fetchSchoolList()

    suspend fun fetchSatScore(dbn: String) = apiHelper.fetchSatScore(dbn)
}