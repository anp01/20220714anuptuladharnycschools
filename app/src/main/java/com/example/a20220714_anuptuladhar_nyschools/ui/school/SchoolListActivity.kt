package com.example.a20220714_anuptuladhar_nyschools.ui.school

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20220714_anuptuladhar_nyschools.R
import com.example.a20220714_anuptuladhar_nyschools.adapter.SchoolListAdapter
import com.example.a20220714_anuptuladhar_nyschools.databinding.ActivitySchoolListBinding
import com.example.a20220714_anuptuladhar_nyschools.others.Resource
import com.example.a20220714_anuptuladhar_nyschools.others.ViewExt.hide
import com.example.a20220714_anuptuladhar_nyschools.others.ViewExt.show
import com.example.a20220714_anuptuladhar_nyschools.ui.details.SchoolDetailsActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SchoolListActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySchoolListBinding
    private lateinit var schoolListAdapter: SchoolListAdapter
    private val mainViewModel: SchoolListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupToolbar()
        setupRecyclerview()
        observeViewModel()
    }

    private fun setupToolbar() {
        supportActionBar?.apply {
            setDisplayShowHomeEnabled(true)
            setDisplayHomeAsUpEnabled(true)
            title = getString(R.string.new_york_school_list)
        }
    }

    private fun observeViewModel() {
        mainViewModel.schoolListResponse.observe(this) { resource ->
            when (resource) {
                is Resource.Loading -> {
                    binding.apply {
                        recyclerView.hide()
                        progressBar.show()
                        errorTextView.hide()
                    }
                }

                is Resource.Success -> {
                    binding.apply {
                        recyclerView.show()
                        progressBar.hide()
                        errorTextView.hide()
                    }

                    schoolListAdapter.submitList(resource.data)
                }

                is Resource.Error -> {
                    binding.apply {
                        recyclerView.hide()
                        progressBar.hide()
                        errorTextView.show()

                        errorTextView.text = resource.message
                    }
                }
            }
        }
    }

    private fun setupRecyclerview() {
        schoolListAdapter = SchoolListAdapter() { selectedSchool ->
            val intent = Intent(this, SchoolDetailsActivity::class.java)

            val bundle = Bundle().also {
                it.putParcelable(SchoolDetailsActivity.SCHOOL_DETAILS_INTENT, selectedSchool)
            }
            intent.putExtras(bundle)
            startActivity(intent)
        }
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@SchoolListActivity)
            setHasFixedSize(true)
            adapter = schoolListAdapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }
}