package com.example.a20220714_anuptuladhar_nyschools.ui.school

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.a20220714_anuptuladhar_nyschools.models.SchoolResponse
import com.example.a20220714_anuptuladhar_nyschools.others.Resource
import com.example.a20220714_anuptuladhar_nyschools.repository.SchoolRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(private val schoolRepository: SchoolRepository) :
    ViewModel() {

    val schoolListResponse = MutableLiveData<Resource<SchoolResponse>>()


    init {
        fetchSchoolList()
    }

    /**
     * Fetch school List via api
     * executed in viewModel initialization
     * Resource class implemented to maintain api call state

     */
    private fun fetchSchoolList() {
        viewModelScope.launch {
            try {
                schoolListResponse.postValue(Resource.Loading())
                val response = schoolRepository.fetchSchoolList()
                if (response.isSuccessful) {
                    schoolListResponse.postValue(Resource.Success(response.body()))
                } else {
                    schoolListResponse.postValue(Resource.Error("Something went wrong."))
                }
            } catch (e: Exception) {
                schoolListResponse.postValue(Resource.Error(e.localizedMessage))
            }
        }
    }
}