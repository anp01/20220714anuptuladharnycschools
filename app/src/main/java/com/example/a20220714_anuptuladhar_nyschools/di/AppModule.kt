package com.example.a20220714_anuptuladhar_nyschools.di

import com.example.a20220714_anuptuladhar_nyschools.BuildConfig
import com.example.a20220714_anuptuladhar_nyschools.api.ApiHelper
import com.example.a20220714_anuptuladhar_nyschools.api.ApiHelperImpl
import com.example.a20220714_anuptuladhar_nyschools.api.ApiService
import com.example.a20220714_anuptuladhar_nyschools.others.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun provideBaseUrl() = Constants.BASE_URL

    /**
     * provide Okhttp client
     * enable http logger if app runs in Debug config
     */
    @Provides
    @Singleton
    fun provideOkhttpClient() = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor().also {
            it.level = HttpLoggingInterceptor.Level.BODY
        }
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    } else
        OkHttpClient.Builder()
            .build()

    /**
     * Provide retrofit instance with Gson Conversion factory for json parsing
     */
    @Provides
    @Singleton
    fun provideRetrofit(baseUrl: String, okHttpClient: OkHttpClient): Retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(baseUrl)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) : ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideApiHelper(apiHelperImpl: ApiHelperImpl): ApiHelper = apiHelperImpl
}