package com.example.a20220714_anuptuladhar_nyschools.others

/**
 * Sealed class to maintain Success, Loading and Error state of api call
 */
sealed class Resource<T>(
    val data: T? = null,
    val message: String? = null
) {
    class Loading<T> : Resource<T>()
    class Error<T>(message: String?, data: T? = null) : Resource<T>(data, message)
    class Success<T>(data: T?) : Resource<T>(data)
}
