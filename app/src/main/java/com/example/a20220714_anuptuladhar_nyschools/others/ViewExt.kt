package com.example.a20220714_anuptuladhar_nyschools.others

import android.view.View

object ViewExt {

    /**
     * Kotlin extension to show view
     */
    fun View.show() {
        this.visibility = View.VISIBLE
    }

    /**
     * Kotlin extension to hide view
     */
    fun View.hide() {
        this.visibility = View.GONE
    }
}