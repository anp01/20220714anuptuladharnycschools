package com.example.a20220714_anuptuladhar_nyschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application(){
}